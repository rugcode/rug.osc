﻿/*
 * Rug.Osc
 *
 * Copyright (C) 2013 Phill Tew (peatew@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Rug.Osc")]
[assembly: AssemblyDescription("Rug.Osc (https://bitbucket.org/rugcode/rug.osc/). Simple, complete, open source OSC implementation for .NET and Mono.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © Phill Tew 2013")]
[assembly: ComVisible(false)]
[assembly: Guid("f25d1825-a5f6-40dd-aceb-011df354a562")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Rug.Osc.Tests, PublicKey=002400000480000094000000060200000024000052534131000400000100010079dddfb55fbd803694a2baee8abb336f1a57e03e29ef8efc35626d1f2fb54bae6113ad7f162a7fe5ca7f792201d6ea9baaa2467f765d7ab19f905c669826681a35bf9f64d3e3df5c1fe6c55c2a36183d05fa8b7731ac2c73b557704bef699525f31ca5e34e1a30435e593be1f7e0adc328599d3c446d0360c4a2932e3c994ab0")]